from flask import Flask, render_template
# from config import Config
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app, resources={r"/": {"origins": "*"}})

@app.route('/', methods=['POST','GET'])
@cross_origin(origin='*')
def hello():
	return render_template("index.html")

if __name__ == "__main__":
	app.run()